from pydantic import BaseModel, EmailStr

class CreateUserRequest(BaseModel):
    username: str
    email: EmailStr
    password: str

class UpdateUserRequest(BaseModel):
    username: str
    email: EmailStr

class UpdateActiveUserRequest(BaseModel):
    is_active: bool
    
class UpdateAdminUserRequest(BaseModel):
    is_admin: bool