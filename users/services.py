from users.models import UserModel
from fastapi.exceptions import HTTPException
from core.security import get_password_hash
from datetime import datetime
from loguru import logger


async def create_user_account(data, db):
    user = db.query(UserModel).filter(UserModel.username == data.username).first()
    if user:
        raise HTTPException(status_code=422, detail="User is already registered with us.")
    user = db.query(UserModel).filter( UserModel.email == data.email).first()
    if user:
        raise HTTPException(status_code=422, detail="Email is already registered with us.")

    new_user = UserModel(
        username=data.username,
        email=data.email,
        password=get_password_hash(data.password),
        is_active=False,
        is_verified=False,
        registered_at=datetime.now(),
        updated_at=datetime.now()
    )
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user
async def delete_user_detail(id, db):
    user = db.query(UserModel).filter(UserModel.id == id).first()
    if not user:
        raise HTTPException(status_code=422, detail="User is not registered")
    
    if not user.is_admin:
        raise HTTPException(status_code=422, detail="User is not admin")
    user.status=True
    user.updated_at=datetime.now()
    db.commit()
async def update_user_detail(id, data, db):
    user = db.query(UserModel).filter(UserModel.id == id).first()
    if not user:
        raise HTTPException(status_code=422, detail="User is not registered")
    
    if not user.is_admin:
        raise HTTPException(status_code=422, detail="User is not a admin")

    user.username=data.username,
    user.email=data.email,
    user.updated_at=datetime.now()
    db.commit()
    user = db.query(UserModel).filter(UserModel.id == id).first()
    return user
async def update_user_isactive(id, data, db):
    user = db.query(UserModel).filter(UserModel.id == id).first()
    if not user:
        raise HTTPException(status_code=422, detail="User is not registered")
    
    if not user.is_admin:
        raise HTTPException(status_code=422, detail="User is not a admin")

    user.is_active=data.is_active
    user.updated_at=datetime.now()
    db.commit()
    user = db.query(UserModel).filter(UserModel.id == id).first()
    return user
async def update_user_isadmin(id, data, db):
    user = db.query(UserModel).filter(UserModel.id == id).first()
    if not user:
        raise HTTPException(status_code=422, detail="User is not registered")

    user.is_admin=data.is_admin
    user.updated_at=datetime.now()
    db.commit()
    user = db.query(UserModel).filter(UserModel.id == id).first()
    return user
async def get_user_detail(id, db):
    user = db.query(UserModel).filter(UserModel.id == id).first()
    return user

async def get_user_detail_all(db):
    users = db.query(UserModel).all()
    return users

