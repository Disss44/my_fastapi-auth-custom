from fastapi import APIRouter, status, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from core.database import get_db
from users.schemas import CreateUserRequest,UpdateUserRequest,UpdateActiveUserRequest,UpdateAdminUserRequest
from users.services import create_user_account,get_user_detail,get_user_detail_all,update_user_detail,delete_user_detail,update_user_isactive,update_user_isadmin
from core.security import oauth2_scheme
from users.responses import UserResponse
from tools.response_tool import response
from fastapi.encoders import jsonable_encoder
from loguru import logger

router = APIRouter(
    prefix="/users",
    tags=["Users"],
    responses={404: {"description": "Not found"}},
    dependencies=[Depends(oauth2_scheme)]
)

@router.post('', status_code=status.HTTP_201_CREATED)
async def create_user(data: CreateUserRequest, db: Session = Depends(get_db)):
    user = await create_user_account(data=data, db=db)
    return response(code=1, msg='ok', data=jsonable_encoder(user))


@router.delete('/{user_id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_user(user_id: str, db: Session = Depends(get_db)):
    # 逻辑删除，改status字段为 1 
    await delete_user_detail(id=user_id, db=db)
    return response(code=1, msg='ok', data='User deleted!')

@router.put('/{user_id}')
async def update_user(user_id: str, data: UpdateUserRequest, db: Session = Depends(get_db)):
    user = await update_user_detail(id=user_id, data=data, db=db)
    return response(code=1, msg='ok', data=jsonable_encoder(user))

@router.put('/active/{user_id}')
async def update_user_active(user_id: str, data: UpdateActiveUserRequest,  db: Session = Depends(get_db)):
    user = await update_user_isactive(id=user_id, data=data, db=db)
    return response(code=1, msg='ok', data=jsonable_encoder(user))

@router.put('/admin/{user_id}')
async def update_user_admin(user_id: str, data: UpdateAdminUserRequest,  db: Session = Depends(get_db)):
    user = await update_user_isadmin(id=user_id, data=data, db=db)
    return response(code=1, msg='ok', data=jsonable_encoder(user))


@router.get('/{user_id}')
async def get_user(user_id: str, db: Session = Depends(get_db)):
    user = await get_user_detail(id=user_id, db=db)
    if user is None:
        return response(code=0, msg='ok', data= 'User not found')
    return response(code=1, msg='ok', data=jsonable_encoder(user))


@router.get('')
async def get_user_all(db: Session = Depends(get_db)):
    users = await get_user_detail_all(db=db)
    if users is None:
        return response(code=0, msg='ok', data= 'User not found')
    return response(code=1, msg='ok', data=jsonable_encoder(users))