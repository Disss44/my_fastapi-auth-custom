from articles.models import ArticleModel
from fastapi.responses import JSONResponse
from fastapi.exceptions import HTTPException
from core.security import get_password_hash
from datetime import datetime
from loguru import logger


async def create_article_detail(data, db):
    article = db.query(ArticleModel).filter(ArticleModel.title == data.title).first()
    if article:
        raise HTTPException(status_code=422, detail="Article is already created with us.")

    new_article = ArticleModel(
        title=data.title,
        content=data.content,
        author_id=data.author_id,
        status=data.status,
        updated_at=datetime.now()
    )
    db.add(new_article)
    db.commit()
    db.refresh(new_article)
    return new_article

async def get_article_detail(id, db):
    article = db.query(ArticleModel).filter(ArticleModel.id == id).first()
    return article

async def delete_article_detail(id, db):
    article = db.query(ArticleModel).filter(ArticleModel.id == id).first()
    if not article:
        raise HTTPException(status_code=422, detail="Article is deleted with us.")
    db.delete(article)
    db.commit()
async def update_article_detail(id, data, db):
    article = db.query(ArticleModel).filter(ArticleModel.id == id).first()
    if not article:
        raise HTTPException(status_code=422, detail="Article is not updated with us.")
    
    article.title=data.title,
    article.content=data.content,
    article.updated_at=datetime.now()
    db.commit()
    article = db.query(ArticleModel).filter(ArticleModel.id == id).first()
    return article
async def get_article_detail_all(db):
    articles = db.query(ArticleModel).all()
    return articles
async def get_article_detail_all(db):
    articles = db.query(ArticleModel).all()
    return articles

