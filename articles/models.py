from sqlalchemy import Column, Enum, ForeignKey, Integer, String, DateTime, Text, func
from datetime import datetime
from sqlalchemy.orm import relationship

from core.database import Base


class ArticleModel(Base):
    __tablename__ = "articles"
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(255), nullable=False)
    content = Column(Text, nullable=False)
    author_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    status = Column(Enum('draft', 'published', 'archived'), default='draft')
    updated_at = Column(DateTime, nullable=True, onupdate=datetime.now)
    created_at = Column(DateTime, nullable=False, server_default=func.now())

    users = relationship("UserModel", back_populates='articles')




















