from pydantic import BaseModel

class CreateArticleRequest(BaseModel):
    author_id: int
    title: str
    content: str
    status: str = 'draft'

class UpdateArticleRequest(BaseModel):
    title: str
    content: str