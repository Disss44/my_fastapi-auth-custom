from typing import Any, Union
from fastapi import APIRouter, Response, status, Depends
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from core.database import get_db
from articles.schemas import CreateArticleRequest,UpdateArticleRequest
from articles.services import create_article_detail, get_article_detail, get_article_detail_all,delete_article_detail,update_article_detail
from core.security import oauth2_scheme
from fastapi.encoders import jsonable_encoder
from tools.response_tool import response
from loguru import logger


router = APIRouter(
    prefix="/articles",
    tags=["Articles"],
    responses={404: {"description": "Not found"}},
    dependencies=[Depends(oauth2_scheme)]
)


@router.post('', status_code=status.HTTP_201_CREATED)
async def create_article(data: CreateArticleRequest, db: Session = Depends(get_db)):
    article = await create_article_detail(data=data, db=db)
    return response(code=1, msg='ok', data=jsonable_encoder(article))

@router.delete('/{article_id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_article(article_id: str, db: Session = Depends(get_db)):
    await delete_article_detail(id=article_id, db=db)
    return response(code=1, msg='ok', data='Article deleted!')

@router.put('/{article_id}')
async def update_article(article_id: str, data: UpdateArticleRequest, db: Session = Depends(get_db)):
    article = await update_article_detail(id=article_id, data=data, db=db)
    if article is None:
        return response(code=0, msg='ok', data= 'Article not found')
    return response(code=1, msg='ok', data=jsonable_encoder(article))

@router.get('/{article_id}')
async def get_article(article_id: str, db: Session = Depends(get_db)):
    article = await get_article_detail(id=article_id, db=db)
    if article is None:
        return response(code=0, msg='ok', data= 'Article not found')
    return response(code=1, msg='ok', data=jsonable_encoder(article))

@router.get('')
async def get_article_all(db: Session = Depends(get_db)):
    articles = await get_article_detail_all(db=db)
    if articles is None:
        return response(code=0, msg='ok', data= 'Article not found')
    return response(code=1, msg='ok', data=jsonable_encoder(articles))