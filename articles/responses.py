from pydantic import BaseModel
from typing import List, Union
from datetime import datetime

class BaseResponse(BaseModel):
    class Config:
        from_attributes = True
        arbitrary_types_allowed = True


class ArticleResponse(BaseModel):
    id: int
    title: str
    content: str
    author_id: int
    status: str
    created_at: Union[None, datetime] = None

class ArticleResponseList(BaseModel):
    articles: List[ArticleResponse]