from typing import Any
from fastapi.responses import JSONResponse


def response(code: int, msg: str, data: Any = None) -> JSONResponse:
    payload = {'code': code, 'msg': msg, 'data': data}
    return JSONResponse(content=payload)
