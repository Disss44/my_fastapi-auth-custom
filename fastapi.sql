--- 创建数据库
CREATE DATABASE `fastapi`;

-- 创建用户表
CREATE TABLE `users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID，自增', 
  `username` VARCHAR(100) NOT NULL COMMENT '用户名，不允许为空',
  `email` VARCHAR(255) NOT NULL UNIQUE COMMENT '用户邮箱，不允许为空，需唯一',
  `password` VARCHAR(100) NOT NULL COMMENT '用户密码，不允许为空',
  `status` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '用户状态，默认为0正常,1为删除',
  `is_active` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '用户是否激活，默认为0（未激活）',
  `is_admin` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '用户是否管理员，默认为0（1为管理员）',
  `is_verified` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '用户是否已验证，默认为0（未验证）',
  `verified_at` DATETIME NULL DEFAULT NULL COMMENT '用户验证时间，可为空', 
  `registered_at` DATETIME NULL DEFAULT NULL COMMENT '用户注册时间，可为空', 
  `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '用户信息更新时间，可为空，更新记录时自动设置为当前时间',
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '用户信息创建时间，不允许为空',
   PRIMARY KEY (`id`)  
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- 创建文章表
CREATE TABLE `articles` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '文章ID，自增',
  `title` VARCHAR(255) NOT NULL COMMENT '文章标题，不允许为空',
  `content` TEXT NOT NULL COMMENT '文章内容，不允许为空',
  `author_id` INT NOT NULL COMMENT '作者ID，不允许为空，外键关联用户表',
  `status` ENUM('draft', 'published', 'archived') NOT NULL DEFAULT 'draft' COMMENT '文章状态，默认为草稿',
  `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '文章信息更新时间，可为空，更新记录时自动设置为当前时间',
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文章创建时间，不允许为空',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`author_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='article表';

--- 初始化用户--管理员
INSERT INTO `users` VALUES (1, 'liang', 'liang@163.com', '$2b$12$a5F0diRm0Pfxm7t65hJQ.uorp1Nvr6lOY.Npa2x10aJHeQWuqFnUO', 1, 1, 1, 0, NULL, '2024-03-17 16:23:01', '2024-03-17 17:03:09', '2024-03-17 16:23:00');
