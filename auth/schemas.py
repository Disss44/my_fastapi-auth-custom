from pydantic import BaseModel, EmailStr

class OAuth2PasswordRequest(BaseModel):
    username: str
    password: str