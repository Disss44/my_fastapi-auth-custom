from fastapi import APIRouter, status, Depends, Header
from sqlalchemy.orm import Session
from core.database import get_db
from auth.services import get_token, get_refresh_token
from auth.schemas import OAuth2PasswordRequest
from tools.response_tool import response
from fastapi.encoders import jsonable_encoder

router = APIRouter(
    prefix="/auth",
    tags=["Auth"],
    responses={404: {"description": "Not found"}},
)

@router.post("/token", status_code=status.HTTP_200_OK)
async def authenticate_user(data: OAuth2PasswordRequest, db: Session = Depends(get_db)):
    token = await get_token(data=data, db=db)
    return response(code=1, msg='ok', data=jsonable_encoder(token))


@router.post("/refresh", status_code=status.HTTP_200_OK)
async def refresh_access_token(refresh_token: str = Header(), db: Session = Depends(get_db)):
    token = await get_refresh_token(token=refresh_token, db=db)
    return response(code=1, msg='ok', data=jsonable_encoder(token))